<?php

/**
 * User policies controller.
 *
 * @category   apps
 * @package    openvpn
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/clearos/clearfoundation/app-openvpn */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * User policies controller.
 *
 * @package    openvpn
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/clearos/clearfoundation/app-openvpn
 */

class Additional_settings extends ClearOS_Controller
{
    /**
     * Settings default controller.
     *
     * @return view
     */

    function index()
    {
        $this->view();
    }

    /**
     * Edit view.
     *
     * @return view
     */

    function edit()
    {
        $this->_item('edit');
    }

    /**
     * View view.
     *
     * @return view
     */

    function view()
    {
        $this->_item('view');
    }

    /**
     * Common view/edit view.
     *
     * @param string $form_type form type
     *
     * @return view
     */

    function _item($form_type)
    {
        // Load libraries
        //---------------

        $this->lang->load('openvpn');
        $this->load->library('openvpn/OpenVPN');

        // Set validation rules
        //---------------------
         
        $this->form_validation->set_policy('nat_enabled', 'openvpn/OpenVPN', 'validate_nat_enabled_state', TRUE);
        $form_ok = $this->form_validation->run();

        // Handle form submit
        //-------------------

        if (($this->input->post('submit') && $form_ok)) {
            try {
                $this->openvpn->set_nat_enabled_state($this->input->post('nat_enabled'));
                $this->openvpn->set_redirect_gateway($this->input->post('redirect_gateway'));
                $this->openvpn->set_client_to_client($this->input->post('client_to_client'));
                $this->openvpn->set_block_outside_dns($this->input->post('block_outside_dns'));

                $this->openvpn->firewall_restart();
                if ($this->openvpn->get_running_state()) {
                    $this->openvpn->set_running_state(false);
                    $this->openvpn->set_running_state(true);
                }

                $this->page->set_status_updated();
                redirect('/openvpn/openvpn');
            } catch (Engine_Exception $e) {
                $this->page->view_exception($e->get_message());
                return;
            }
        }

        // Load view data
        //---------------

        try {
            $data['form_type'] = $form_type;
            $data['nat_enabled'] = $this->openvpn->get_nat_enabled_state();
            $data['redirect_gateway'] = $this->openvpn->get_redirect_gateway();
            $data['client_to_client'] = $this->openvpn->get_client_to_client();
            $data['block_outside_dns'] = $this->openvpn->get_block_outside_dns();
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('openvpn/additional_settings', $data, lang('openvpn_additional_settings'));
    }
}
