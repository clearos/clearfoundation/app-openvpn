<?php

/**
 * OpenVPN general settings view.
 *
 * @category   apps
 * @package    openvpn
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/smtp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('openvpn');
$this->lang->load('base');

///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

if ($form_type === 'edit') {
    $read_only = FALSE;
    $buttons = array(
        form_submit_update('submit'),
        anchor_cancel('/app/openvpn')
    );
} else {
    $read_only = TRUE;
    $buttons = array(anchor_edit('/app/openvpn/additional_settings/edit'));
}

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

echo form_open('/openvpn/additional_settings/edit');
echo form_header(lang('openvpn_additional_settings'));

echo field_toggle_enable_disable('nat_enabled', $nat_enabled, lang('openvpn_nat_enabled'), $read_only);
echo field_toggle_enable_disable('redirect_gateway', $redirect_gateway, lang('openvpn_redirect_gateway'), $read_only);
echo field_toggle_enable_disable('client_to_client', $client_to_client, lang('openvpn_client_to_client'), $read_only);
echo field_toggle_enable_disable('block_outside_dns', $block_outside_dns, lang('openvpn_block_outside_dns'), $read_only);
echo field_button_set($buttons);

echo form_footer();
echo form_close();
